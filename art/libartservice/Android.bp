// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "art_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    default_applicable_licenses: ["art_license"],
}

cc_library {
    // This native library contains JNI support code for the ART Service Java
    // Language library.

    name: "libartservice",
    defaults: ["art_defaults"],
    host_supported: true,
    srcs: [
        "service/native/service.cc",
    ],
    export_include_dirs: ["."],
    apex_available: [
        "com.android.art",
        "com.android.art.debug",
    ],
    shared_libs: [
        "libbase",
    ],
    export_shared_lib_headers: ["libbase"],
}

java_library {
    // This Java Language Library contains the ART Service class that will be
    // loaded by the System Server.

    name: "artservice",
    visibility: [
        "//art:__subpackages__",
    ],

    apex_available: [
        "com.android.art",
        "com.android.art.debug",
    ],
    sdk_version: "core_platform",
    min_sdk_version: "31",

    srcs: [
        "service/java/com/android/server/art/ArtService.java",
    ],

    libs: [
        "art.module.api.annotations.for.system.modules",
        "unsupportedappusage",
    ],

    plugins: ["java_api_finder"],
}

art_cc_test {
    name: "art_libartservice_tests",
    defaults: [
        "art_gtest_defaults",
    ],
    srcs: [
        "service/native/service_test.cc",
    ],
    shared_libs: [
        "libbase",
        "libartservice",
    ],
}
