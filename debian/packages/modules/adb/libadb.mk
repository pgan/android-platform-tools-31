NAME := libadb

LIBADB_SRC_FILES := \
  adb.cpp \
  adb_io.cpp \
  adb_listeners.cpp \
  adb_trace.cpp \
  adb_unique_fd.cpp \
  adb_utils.cpp \
  fdevent/fdevent.cpp \
  services.cpp \
  sockets.cpp \
  socket_spec.cpp \
  sysdeps/env.cpp \
  sysdeps/errno.cpp \
  transport.cpp \
  transport_fd.cpp \
  types.cpp \

LIBADB_posix_srcs := \
  sysdeps_unix.cpp \
  sysdeps/posix/network.cpp \

LIBADB_linux_SRC_FILES := \
  fdevent/fdevent_epoll.cpp \
  client/auth.cpp \
  client/usb_libusb.cpp \
  client/usb_dispatch.cpp \
  client/transport_local.cpp \
  client/transport_usb.cpp \
  client/usb_linux.cpp \

LOCAL_SRC_FILES := \
  $(LIBADB_SRC_FILES) \
  $(LIBADB_posix_srcs) \
  $(LIBADB_linux_SRC_FILES) \

LIBDIAGNOSE_USB_SRC_FILES = system/core/diagnose_usb/diagnose_usb.cpp

GEN := debian/out/packages/modules/adb/transport_mdns_unsupported.cpp

SOURCES := $(GEN) \
           $(foreach source, $(LOCAL_SRC_FILES), packages/modules/adb/$(source)) \
           $(LIBDIAGNOSE_USB_SRC_FILES)
OBJECTS = $(SOURCES:.cpp=.o)

LIBADB_PROTO := \
  key_type.proto \
  app_processes.proto \

SOURCES_PROTO = $(LIBADB_PROTO:.proto=.pb.cc)
SOURCES_PROTO := $(foreach source, $(SOURCES_PROTO), debian/out/packages/modules/adb/proto/$(source))
OBJECTS_PROTO = $(SOURCES_PROTO:.cc=.o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
            -I/usr/include/android \
            -Ipackages/modules/adb \
            -Ipackages/modules/adb/crypto/include \
            -Ipackages/modules/adb/tls/include \
            -Isystem/libbase/include \
            -Isystem/core/diagnose_usb/include \
            -Isystem/core/libcrypto_utils/include \
            -Isystem/core/include \
            -Iexternal/boringssl/include \
            -Isystem/core/libcutils/include \
            -Idebian/out/packages/modules/adb/proto \
            -DPLATFORM_TOOLS_VERSION='"$(PLATFORM_TOOLS_VERSION)"' \
            -DADB_HOST=1 -DADB_VERSION='"$(DEB_VERSION)"'

debian/out/packages/modules/adb/$(NAME).a: $(OBJECTS_PROTO) $(OBJECTS)
	mkdir --parents debian/out/packages/modules/adb
	ar -rcs $@ $^

$(OBJECTS_PROTO): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

$(SOURCES_PROTO): debian/out/%.pb.cc: %.proto
	mkdir --parents debian/out/packages/modules/adb/proto
	protoc --cpp_out=debian/out/packages/modules/adb/proto \
	       --proto_path=packages/modules/adb/proto $<

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

debian/out/packages/modules/adb/transport_mdns_unsupported.cpp:
	echo 'void init_mdns_transport_discovery(void) {}' > $@

