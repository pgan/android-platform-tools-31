NAME = adb

SOURCES = \
  client/adb_client.cpp \
  client/bugreport.cpp \
  client/commandline.cpp \
  client/file_sync_client.cpp \
  client/main.cpp \
  client/console.cpp \
  client/adb_install.cpp \
  client/line_printer.cpp \
  client/fastdeploy.cpp \
  client/fastdeploycallbacks.cpp \
  client/incremental.cpp \
  client/incremental_server.cpp \
  client/incremental_utils.cpp \
  shell_service_protocol.cpp \

SOURCES := $(foreach source, $(SOURCES), packages/modules/adb/$(source))
OBJECTS = $(SOURCES:.cpp=.o)

FASTDEPLOY_PROTO := \
  ApkEntry.proto \

SOURCES_PROTO = $(FASTDEPLOY_PROTO:.proto=.pb.cc)
SOURCES_PROTO := $(foreach source, $(SOURCES_PROTO), debian/out/packages/modules/adb/fastdeploy/proto/$(source))
OBJECTS_PROTO = $(SOURCES_PROTO:.cc=.o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
  -I/usr/include/android \
  -Isystem/core/include \
  -Ipackages/modules/adb \
  -Idebian/out/packages/modules/adb/proto \
  -Idebian/out/packages/modules/adb \
  -Isystem/libbase/include \
  -Iexternal/boringssl/include \
  -DADB_VERSION='"$(DEB_VERSION)"' -DADB_HOST=1 -D_GNU_SOURCE \

LDFLAGS += -lpthread -lusb-1.0
STATIC_LIBS = \
  debian/out/packages/modules/adb/libadb.a \
  debian/out/system/core/libcutils.a \
  debian/out/system/core/libbase.a \
  debian/out/system/core/liblog.a \
  debian/out/system/core/libcrypto_utils.a \
  debian/out/external/boringssl/libcrypto.a \

# -latomic should be the last library specified
# https://github.com/android/ndk/issues/589
ifneq ($(filter armel mipsel,$(DEB_HOST_ARCH)),)
  LDFLAGS += -latomic
endif

debian/out/packages/modules/adb/$(NAME): $(OBJECTS_PROTO) $(OBJECTS)
	mkdir --parents debian/out/packages/modules/adb
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CPPFLAGS) $(STATIC_LIBS) $(LDFLAGS)

$(OBJECTS_PROTO): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

$(SOURCES_PROTO): debian/out/%.pb.cc: %.proto
	mkdir --parents debian/out/packages/modules/adb/fastdeploy/proto
	protoc --cpp_out=debian/out/packages/modules/adb/fastdeploy/proto \
	       --proto_path=packages/modules/adb/fastdeploy/proto $<

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
