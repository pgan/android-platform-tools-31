NAME:= libcrypto_utils

SOURCES = system/core/libcrypto_utils/android_pubkey.cpp
OBJECTS = $(SOURCES:.cpp=.o)

CPPFLAGS += -I/usr/include/android \
            -Isystem/core/libcrypto_utils/include \
            -Isystem/core/include \
            -Iexternal/boringssl/include \

debian/out/system/core/$(NAME).a: $(OBJECTS)
	mkdir --parents debian/out/system/core
	ar -rcs $@ $^

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CPPFLAGS) $(CXXFLAGS)
