include /usr/share/dpkg/architecture.mk

NAME = libutils

SOURCES = \
  CallStack.cpp \
  Looper.cpp \
  ProcessCallStack.cpp \
  \
  Errors.cpp \
  FileMap.cpp \
  JenkinsHash.cpp \
  NativeHandle.cpp \
  Printer.cpp \
  RefBase.cpp \
  SharedBuffer.cpp \
  StopWatch.cpp \
  String8.cpp \
  String16.cpp \
  StrongPointer.cpp \
  SystemClock.cpp \
  Threads.cpp \
  Timers.cpp \
  Tokenizer.cpp \
  Unicode.cpp \
  VectorImpl.cpp \
  misc.cpp \

SOURCES := $(foreach source, $(SOURCES), system/core/libutils/$(source))
OBJECTS = $(SOURCES:.cpp=.o)

CXXFLAGS += -std=gnu++17
CPPFLAGS += \
            -I/usr/include/android \
            -Isystem/core/include \
            -Isystem/libbase/include \
            -Isystem/core/cutils/include \
            -Isystem/core/libprocessgroup/include \
            -Isystem/unwinding/libbacktrace/include \
            -Isystem/logging/liblog/include \
            -Isystem/core/libcutils/include \
            -DLIBUTILS_NATIVE=1 \

debian/out/system/core/$(NAME).a: $(OBJECTS)
	mkdir --parents debian/out/system/core
	ar -rcs $@ $^

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
