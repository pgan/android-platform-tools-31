NAME = libziparchive

SOURCES = \
  zip_archive.cc \
  zip_archive_stream_entry.cc \
  zip_cd_entry_map.cc \
  zip_writer.cc \

SOURCES_err = zip_error.cpp

SOURCES := $(foreach source, $(SOURCES), system/libziparchive/$(source))
OBJECTS = $(SOURCES:.cc=.o)
SOURCES_err := $(foreach source, $(SOURCES_err), system/libziparchive/$(source))
OBJECTS_err = $(SOURCES_err:.cpp=.o)

CXXFLAGS += -std=gnu++17
CPPFLAGS += \
  -DZLIB_CONST -D_FILE_OFFSET_BITS=64 \
  -I/usr/include/android \
  -Isystem/core/include \
  -Isystem/libbase/include \
  -Isystem/libziparchive/include \
  -Isystem/logging/liblog/include \

debian/out/system/libziparchive/$(NAME).a: $(OBJECTS) $(OBJECTS_err)
	mkdir --parents debian/out/system/libziparchive
	ar -rcs $@ $^

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

$(OBJECTS_err): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
